example of how to setup a gitlab external secret

```yaml
---
apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: nextcloud-admin-credentials
spec:
  refreshInterval: 1h
  secretStoreRef:
  kind: SecretStore
  # Must match SecretStore on the cluster
  name: gitlab-secret-store

  target:
    # Name for the secret to be created on the cluster
  data:
    # Key given to the secret to be created on the cluster
    - secretKey: username
      # Key of the variable on Gitlab
      remoteRef:
        key: nextcloud_username
    - secretKey: password
      # Key of the variable on Gitlab
      remoteRef:
        key: nextcloud_password
    - secretKey: serverinfo_token
      # Key of the variable on Gitlab
      remoteRef:
        key: nextcloud_serverinfo_token
```
