Example of how to use gitlab secrets

```yaml
---
apiVersion: external-secrets.io/v1beta1
kind: SecretStore
metadata:
  name: gitlab-secret-store
  namespace: nextcloud
spec:
  provider:
    gitlab:
      auth:
        SecretRef:
          accessToken:
            name: gitlab-secret
            key: token
      projectID: "38741778"
```
